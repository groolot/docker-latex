FROM debian:sid-slim

RUN apt-get update && apt-get install -y \
  texlive \
  texlive-bibtex-extra \
  texlive-extra-utils \
  texlive-font-utils \
  texlive-fonts-extra \
  texlive-fonts-extra-links \
  texlive-fonts-recommended \
  texlive-formats-extra \
  texlive-games \
  texlive-generic-extra \
  texlive-generic-recommended \
  texlive-humanities \
  texlive-lang-english \
  texlive-lang-french \
  texlive-latex-extra \
  texlive-latex-recommended \
  texlive-metapost \
  texlive-music \
  texlive-pictures \
  texlive-pstricks \
  texlive-publishers \
  texlive-science

# some auxiliary tools
RUN apt-get update && apt-get install -y \
  fig2dev \
  git \
  gnupg \
  graphviz \
  latexmk \
  make \
  openssh-client \
  pandoc \
  pandoc-citeproc \
  python3-pygments \
  wget \
  && \
  # Removing documentation packages *after* installing them is kind of hacky,
  # but it only adds some overhead while building the image.
  apt-get --purge remove -y .\*-doc$ && \
  # Remove more unnecessary stuff
  apt-get clean -y && \
  apt-get autoremove -y
